#include <iostream>
#include <fstream>
using namespace std;

struct kontener {
	kontener *nastepny;
	int zawartosc;
};
class ListaJednokierunkowa {
public:
	kontener *poczatekListy;
	kontener *koniecListy;
	ListaJednokierunkowa();

	bool jestPusta();
	void dodaj(int dane);
	void wypisz();
	void zeruj();
};
ListaJednokierunkowa::ListaJednokierunkowa() {
	poczatekListy = NULL;
	koniecListy = NULL;
}
bool ListaJednokierunkowa::jestPusta() {
	if (poczatekListy == NULL) {
		return true;
	}
	return false;
}
void ListaJednokierunkowa::dodaj(int dane) {
	kontener *nowyElement = new kontener();
	nowyElement->zawartosc = dane;

	if (jestPusta()) {
		nowyElement->nastepny = NULL;

		poczatekListy = nowyElement;
		koniecListy = nowyElement;
	}
	else {
		koniecListy->nastepny = nowyElement;
		nowyElement->nastepny = NULL;
		koniecListy = nowyElement;
	}
}
void ListaJednokierunkowa::wypisz() {
	if (poczatekListy != NULL) {
		kontener *tmp = poczatekListy;
		while (tmp != koniecListy)
		{
			cout << tmp->zawartosc;
			tmp = tmp->nastepny;
		}
		cout << tmp->zawartosc << endl;
	}
	else {
		cout << endl;
	}
	return;
}
void ListaJednokierunkowa::zeruj() {
	if (poczatekListy != NULL) {
		kontener *tmp = poczatekListy;
		kontener *tmp2 = poczatekListy->nastepny;

		while (tmp != koniecListy)
		{
			delete tmp;
			tmp = tmp2;
			tmp2 = tmp2->nastepny;
		}
		delete tmp;
		poczatekListy = NULL;
		koniecListy = NULL;
	}
}

class kopiec {
public:
	int *tab;
	int rozmiar;
	kopiec(int iloscKoralowcow);
	void dodaj(int v, int *waga);
	int usun_minimum(int *waga);
};

//Konstruktor - tworzy pusty kopiec
kopiec::kopiec(int iloscKoralowcow) {
	rozmiar = 0;
	tab = new int[iloscKoralowcow];
}

//Wstawia element o wartosci v do kopca
void kopiec::dodaj(int numer, int *waga) {
	tab[rozmiar + 1] = numer;
	int s = rozmiar + 1;
	while (s != 1) {
		if (waga[tab[s / 2]] < waga[tab[s]]) {
			swap(tab[s / 2], tab[s]);
			s = s / 2;
		}
		else
			break;
	}
	rozmiar++;
}

//usuwanie liscia 
int kopiec::usun_minimum(int *waga) {
	rozmiar--;
	return tab[rozmiar + 1];
}

//Usuwa korzen kopca
//int kopiec::usun_minimum(int *waga) {
//	int zwroc;
//	zwroc = tab[1];
//
//	tab[1] = tab[rozmiar];
//	rozmiar--;
//	int tmp = 1;
//	while (tmp * 2 <= rozmiar) {
//		if (waga[tab[tmp]] < waga[tab[tmp * 2]] || waga[tab[tmp]] < waga[tab[tmp * 2 + 1]]) {
//			if (waga[tab[tmp * 2]] > waga[tab[tmp * 2 + 1]] || tmp * 2 + 1 > rozmiar) {
//				swap(tab[tmp], tab[tmp * 2]); tmp = tmp * 2;
//			}
//			else {
//				swap(tab[tmp], tab[tmp * 2 + 1]);
//				tmp = tmp * 2 + 1;
//			}
//		}
//		else
//			break;
//	}
//
//	return zwroc;
//}

//Wyswietla zawartosc kopca poczawszy od elementu poczatek
//void kopiec::wyswietl(int poczatek) {
//	if (poczatek <= rozmiar) {
//		cout << poczatek << " : " << tab[poczatek] << " \n";
//		if (poczatek * 2 <= rozmiar) wyswietl(poczatek * 2);
//		if (poczatek * 2 + 1 <= rozmiar) wyswietl(poczatek * 2 + 1);
//	}
//}


void algorytm(ListaJednokierunkowa *LS, int *waga, int *deg, bool *odwiedzone, int iloscKoralowcow) {
	//ogolny przebieg algorytmu jest taki ze najpierw znajdujemy wszystkie liscie i dodaje do kolejki
	//kolejka musi byc posortowana, nie ma tutaj jednak sortowania, po prostu kazdy element jest wstawiany na odpowiednie miejsce 
	//tak aby wyszla lista posortowana wzgledem wagi
	//potem od kazdego liscia przechodze do jego sasiada
	//dodaje wartosc liscia do wartosci sasiada
	//i usuwam ten lisc ( poprzez zmniejszenie stopnia )
	//przez to graf stopniowo sie redukuje

	//tutaj jest kopiec z lisciami czyli wierzcholkami o stopniu 1
	kopiec Q(iloscKoralowcow);

	//cout << "wczytanie lisci do kolejki" << endl;
	for (int i = 0; i < iloscKoralowcow; ++i) {
		if (deg[i] == 1) {
			Q.dodaj(i, waga);
			//kolejka.dodaj(i, waga);
		}
	}

	int	min,
		max = 0,
		numerMax,
		aktualny;


	aktualny = Q.usun_minimum(waga);
	//przechodzenie wszystkich wierzcholkow oprocz jednego ( ostatniego wedlog ktorego bedzie podzial )
	while (Q.rozmiar != 0) {
		//przegladanie sasiadow 
		for (kontener *wsk = LS[aktualny].poczatekListy;
			wsk != NULL;
			wsk = wsk->nastepny)

		{
			//cout << "rozpatruje wierzcholek: " << aktualny << " o wadze: "<< waga[aktualny] <<" deg: "<<deg[aktualny] << endl;
			//WSK->zawartosc == AKTUALNIE ROZPATRYWANY SASIAD

			//sprawdzenie czy sasiad byl juz odwiedzony
			if (odwiedzone[wsk->zawartosc] == false) {

				//zwiekszenie wagi sasiada o wage aktualnie rozpatrywanego liscia
				waga[wsk->zawartosc] += waga[aktualny];
				//zmniejszenie stopnia sasiada ( bo usuwasz lisc )
				deg[wsk->zawartosc]--;

				//jesli po usunieciu aktualnego liscia jego sasiad bedzie lisciem, dodaj trzeba dodac go do kolejki
				if (deg[wsk->zawartosc] == 1)
					Q.dodaj(wsk->zawartosc, waga);
				//kolejka.dodaj(wsk->zawartosc, waga);

				//tutaj zapisuje wartosc maxymalna jaka do tej pory udalo mi sie znalezc 
				//bo krawedzie ktore tworza podzial sa zawsze miedzy wartoscia maxymalna a druga najwieksza w zredukowanym grafie
				if (waga[wsk->zawartosc] > max) {
					max = waga[wsk->zawartosc];
					numerMax = wsk->zawartosc;
				}
			}
		}

		//oznaczenie rozpatrywanego jako odwiedzony
		odwiedzone[aktualny] = true;
		//zmniejszenie stopnia rozpatrywanego wierzcholka
		deg[aktualny]--;

		//tutaj zapisuje przedostatni rozpatrywany wierzcholek, przy ostatnim wierzcholku funkcja zrwoci maxymalna wartos int zeby zakomunikowac ze nie ma juz elementow
		min = waga[aktualny];
		//aktualny = kolejka.usunPierwszy();
		aktualny = Q.usun_minimum(waga);
	}

	//cout << "numerMax: " << numerMax << " max: " << max << endl;
	//cout << "numerMin: " << aktualny << " min: " << min << endl;
	//cout << "koniec cz1" << endl;

	//w patli zliczam ile jest krawedzi kore wychodza od najwiekszej wartosci i maja minimalna wartosc
	int ilosc = 0;
	for (kontener *wsk = LS[numerMax].poczatekListy;
		wsk != NULL;
		wsk = wsk->nastepny)
	{
		if (waga[wsk->zawartosc] == min)
			ilosc++;
	}
	//odejmuje 2xmin bo przy redukcji grafu dodalem do maxymalnej wartosci wierzcholka min, wiec musze to teraz odjac 
	//a drugi raz bo potrzebujemy wypisac roznice miedzy min a max a nie wartosc max
	cout << max - 2 * min << " ";
	cout << ilosc << endl;

	//przegladam wszystkich sasiadow maxymalnego wierzcholka i wypisuje te krawedzie ktore maja wartosc min
	for (kontener *wsk = LS[numerMax].poczatekListy;
		wsk != NULL;
		wsk = wsk->nastepny)
	{
		if (waga[wsk->zawartosc] == min)
		{
			//ten if else jest po to zeby zawsze na poczatku wypisac mniejsza wartosc potem wieksza
			if (wsk->zawartosc < numerMax)
				cout << wsk->zawartosc << " " << numerMax << endl;
			else
				cout << numerMax << " " << wsk->zawartosc << endl;
		}
	}

	return;
}
int main()
{
	//12, 14
	//cout << "start programu" << endl;

	//--------------------- odkomentowujac ten bloczek  tekstu wlaczysz odczyt z pliku
	//std::ifstream in("25.in");
	//std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
	//std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

	//std::ofstream out("25.txt");
	//std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
	//std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
	//----------------------------------------------------------------------------------

	int iloscKoralowcow,
		doKtoregoDokleic;

	cin >> iloscKoralowcow;

	//stworzenie tablicy list jednokierunkowych,
	//co to jest lista sasiedzctwa znajdziesz w wykladach oceta 
	//wyglada to tak:
	//0: 1, 2, 3
	//1: 0, 4
	//co oznacza ze z wierzcholka 0 dojdziesz do 1,2,3 a z wierzcholka 1 do 0 i 4
	ListaJednokierunkowa *LS = new ListaJednokierunkowa[iloscKoralowcow]();

	//jednowymiarowa tablica w ktorej przechowujesz stopien wierzcholka czyli to ile masz sasiadow
	int  *deg = new int[iloscKoralowcow]();
	//waga kazdego wierzcholka
	int	 *waga = new int[iloscKoralowcow];
	//oznaczenie wierzcholkow ktore juz zostaly odwiedzone
	bool *odwiedzone = new bool[iloscKoralowcow]();

	//wczytanie wagi zerowego wierzcholka
	cin >> waga[0];

	for (int i = 1; i<iloscKoralowcow; ++i) {
		//wczytanie do tabeli wag
		cin >> waga[i] >> doKtoregoDokleic;

		//znalezienie minimum
		//dodanie do Listy sasiedzctwa
		//musisz dodac dwie krawedzi z 0 do 1, z 1 do 0
		LS[i].dodaj(doKtoregoDokleic);
		LS[doKtoregoDokleic].dodaj(i);

		//zwiekszenie stopnia wierzcholkow
		//tak samo zwiekszasz dla obu bo krawedz dochodzi do obu wierzcholkow
		deg[i]++;
		deg[doKtoregoDokleic]++;
	}

	//cout << "koniec wczytania danych" << endl;
	algorytm(LS, waga, deg, odwiedzone, iloscKoralowcow);

	//cout << "zwolnienie pamieci" << endl;
	//zwolnienie pamieci;
	for (int i = 0; i < iloscKoralowcow; ++i)
		LS[i].zeruj();

	delete[] LS;
	delete[] deg;
	delete[] waga;
	delete[] odwiedzone;
	//cout << "koniec zwolnienia" << endl;

	//system("pause");
	return 0;
}